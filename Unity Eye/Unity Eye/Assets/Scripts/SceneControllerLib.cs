﻿using System.Collections;
using System.Collections.Generic;
using Tobii.XR.Examples;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneControllerLib : MonoBehaviour
{
    private GlobalControl global;
    private MoveImage move;

    private HiResScreenShots hiRes;

    private List<GazeData> gazeOrder;

    private bool timerStarted = false;
    [HideInInspector]
    public float timeLeft;
    [HideInInspector]
    public float testTime;

    [SerializeField] Button begin;
    [SerializeField] Button save;
    [SerializeField] Button exit;
    [SerializeField] Button setTime;
    [SerializeField] GameObject cubeHolder;
    [SerializeField] Text inputTime;
    [SerializeField] GameObject shelves;
    [SerializeField] Text currentTestDuration;

    // Start is called before the first frame update
    void Start()
    {
        //timeLeft = 30f;
        gazeOrder = new List<GazeData>();
        
        hiRes = GameObject.Find("ResultCamera").GetComponent<HiResScreenShots>();
        global = GameObject.Find("GlobalData").GetComponent<GlobalControl>();
        move = GameObject.Find("SceneController").GetComponent<MoveImage>();

        begin.interactable = true;
        save.interactable = false;
        cubeHolder.SetActive(false);

        timeLeft = global.GetTime();
        testTime = global.GetTime();

        currentTestDuration.text = "Current Test Duration: " + global.GetTime() + " seconds";
    }
    //timer for test
    private void Update()
    {
        if (timerStarted)
        {
            timeLeft -= Time.deltaTime;
            float formatted = Mathf.Round(timeLeft * 100f) / 100f;
            currentTestDuration.text = "Current Test Duration: " + formatted + " seconds";
            if (timeLeft < 0)
            {
                timerStarted = false;
                SaveResults();
            }
        }
    }
    //begin timer
    public void StartTimer()
    {
        timerStarted = true;
    }
    //set timer to value in input field UI element
    public void SetTimer()
    {
        try
        {
            float inputToFloat = float.Parse(inputTime.text);
            timeLeft = inputToFloat;
            testTime = inputToFloat;
            currentTestDuration.text = "Current Test Duration: " + inputTime.text + " seconds";
        }
        catch
        {

        }
    }
    //start test by displaying image to user and activating the heatmap
    public void DisplayImage()
    {
        setTime.interactable = false;
        begin.interactable = false;

        cubeHolder.SetActive(true);
        StartTimer();
    }
    //close program
    public void ExitProgram()
    {
        Application.Quit();
    }
    //save screenshots and spreadsheets
    public void SaveResults()
    {
        setTime.interactable = false;
        begin.interactable = false;
        save.interactable = false;
        hiRes.TakeHiResShot();

        //spreadsheet
        //string parent = drag.GetParent();
        //heatmap spreadsheet
        var heatSheet = new ES3Spreadsheet();
        for (int row = 1; row <= 40; row++)
        {
            for (int col = 1; col <= 40; col++)
            {

                GameObject temp = GameObject.Find("(" + row + "," + col + ")");
                //Debug.Log("(" + col + "," + row + ")");
                float totalTime = temp.GetComponent<HighlightAtGaze>().GetTime();

                //sheet.SetCell<string>(col-1, row-1, "(" + row + "," + col + "): " + totalTime.ToString());    with name of cube
                heatSheet.SetCell<string>(col - 1, row - 1, totalTime.ToString());
            }
        }

        //heatSheet.Save(parent + "/" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "_HeatmapData.csv");
        //gaze order spreadsheet
        var orderSheet = new ES3Spreadsheet();
        int orderRow = 0;
        foreach (GazeData gaze in gazeOrder)
        {
            int rowNum = orderRow + 1;
            orderSheet.SetCell<string>(0, orderRow, rowNum.ToString());
            orderSheet.SetCell<string>(1, orderRow, gaze.Name);
            orderSheet.SetCell<string>(2, orderRow, gaze.Time.ToString());
            orderRow++;
        }

        //orderSheet.Save(parent + "/" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + "_GazeOrderData.csv");

        shelves.SetActive(true);
    }
    //add a cube to gaze order sheet
    public void AddToGazeList(string name, float time)
    {
        GazeData newEntry = new GazeData();
        newEntry.Name = name;
        newEntry.Time = time;

        gazeOrder.Add(newEntry);
    }
    //reset testing environment
    public void ResetScene()
    {
        global.SetTime(testTime);
        global.SetDist(move.currentDistance);
        SceneManager.LoadScene("Main");
    }
    //gaze struct for list
    [System.Serializable]
    public struct GazeData
    {
        public string Name;
        public float Time;
    }
    //get currently set duration of test
    public float GetTime()
    {
        return testTime;
    }
}
