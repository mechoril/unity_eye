﻿using UnityEngine;
using System.Collections;
using Tobii.XR.Examples;

//This script handles taking screenshots of the grid when a test finishes.
public class HiResScreenShots : MonoBehaviour
{
    public int resWidth = 2550;
    public int resHeight = 3300;

    private bool takeHiResShot = false;
    private bool cubesToggled = false;
    private int photoCount = 0;

    [SerializeField]
    Canvas image;

    private static int counter = 0;

    //Get parent file path and name the screenshot
    public static string ScreenShotName(int width, int height)
    {
        ImageExample drag = GameObject.Find("DragAndDrop").GetComponent<ImageExample>();
        string parent = drag.GetParent();
        counter++;
        return string.Format(parent + "/screen_{1}x{2}_{3}"+ counter +".png",
                             Application.dataPath,
                             width, height,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }
    //set bool to true so screenshot will be taken
    public void TakeHiResShot()
    {
        takeHiResShot = true;
    }
    //take 3 screenshots in a row
    void LateUpdate()
    {
        takeHiResShot |= Input.GetKeyDown("k");
        if (takeHiResShot)
        {
            photoCount++;
            RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
            GetComponent<Camera>().targetTexture = rt;
            Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
            GetComponent<Camera>().Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
            GetComponent<Camera>().targetTexture = null;
            RenderTexture.active = null; // JC: added to avoid errors
            Destroy(rt);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(resWidth, resHeight);
            System.IO.File.WriteAllBytes(filename, bytes);
            takeHiResShot = false;
            if (photoCount == 1)
            {
                image.GetComponent<CanvasGroup>().alpha = .15f;
                takeHiResShot = true;
            }
            else if (photoCount == 2)
            {
                image.GetComponent<CanvasGroup>().alpha = 0f;
                takeHiResShot = true;
            }
            else
            {
                image.GetComponent<CanvasGroup>().alpha = 1f;
            }
        }
    }
}