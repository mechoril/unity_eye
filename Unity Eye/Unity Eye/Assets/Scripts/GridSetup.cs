﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//This is useful if in the future, different grids need to be made. For example, if you want a more detailed heatmap, just duplicate the cube prefab adjust it's size, and add the
//GridSetup script to the scene, and adjust the values in the script.This will create a new heatmap that you can save as a prefab when you play the scene.
public class GridSetup : MonoBehaviour
{
    [SerializeField]
    GameObject cube;

    // Start is called before the first frame update
    void Start()
    {
        GameObject parent = new GameObject();
        parent.name = "Heatmap";

        float x = 0;
        float y = 0;
        //create a 40X40 heatmap
        for(int i = 40; i >=1; i--)
        {
            for (int j = 1; j <= 16; j++)
            {
                //create location new cube should spawn
                Vector3 tempVec = new Vector3();
                tempVec.x = x;
                tempVec.y = y;
                tempVec.z = 0;
                //create new cube, name it, and attach it to the parent transform
                GameObject temp =  Instantiate(cube, tempVec, Quaternion.identity);
                temp.name = "(" + i + "," + j + ")";
                temp.transform.SetParent(parent.transform);
                //adjust x value of cube spawn, should be size of the cube
                x -= .05f;
            }
            x = 0;
            //adjust y value of cube spawn, should be size of the cube
            y += .05f;
        }
    }
}
