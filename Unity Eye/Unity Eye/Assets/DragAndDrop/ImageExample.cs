using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using B83.Win32;
using UnityEngine.UI;
using System.IO;

public class ImageExample : MonoBehaviour
{
    Texture2D[] textures = new Texture2D[6];
    DropInfo dropInfo = null;
    [SerializeField]
    Canvas myCanvas = null;
    [SerializeField]
    Button begin = null;
    [SerializeField]
    Button save = null;
    private bool loaded = false;

    private static string parentDirectory = null;

    class DropInfo
    {
        public string file;
        public Vector2 pos;
    }
    void OnEnable ()
    {
        UnityDragAndDropHook.InstallHook();
        UnityDragAndDropHook.OnDroppedFiles += OnFiles;

    }
    void OnDisable()
    {
        UnityDragAndDropHook.UninstallHook();
    }

    void OnFiles(List<string> aFiles, POINT aPos)
    {
        string file = "";
        // scan through dropped files and filter out supported image types
        foreach(var f in aFiles)
        {
            var fi = new System.IO.FileInfo(f);
            var ext = fi.Extension.ToLower();
            if (ext == ".png" || ext == ".jpg" || ext == ".jpeg")
            {
                
                file = f;
                GetParentDir(file);
                break;
            }
        }
        // If the user dropped a supported file, create a DropInfo
        if (file != "")
        {
            var info = new DropInfo
            {
                file = file,
                pos = new Vector2(aPos.x, aPos.y)
            };
            dropInfo = info;
        }
    }

    void LoadImage(int aIndex, DropInfo aInfo)
    {
        if (aInfo == null)
            return;
        // get the GUI rect of the last Label / box
        var rect = GUILayoutUtility.GetLastRect();
        // check if the drop position is inside that rect
        if (rect.Contains(aInfo.pos) && !loaded)
        {
            var data = System.IO.File.ReadAllBytes(aInfo.file);
            var tex = new Texture2D(1,1);
            tex.LoadImage(data);
            if (textures[aIndex] != null)
                Destroy(textures[aIndex]);
            textures[aIndex] = tex;

            begin.interactable = true;
            save.interactable = true;
            loaded = true;
        }
    }

    private void OnGUI()
    {
        DropInfo tmp = null;
        if (Event.current.type == EventType.Repaint && dropInfo!= null)
        {
            tmp = dropInfo;
            dropInfo = null;
        }
        if (textures[0] != null)
            GUILayout.Label(textures[0], GUILayout.Width(200), GUILayout.Height(200));
        else
            GUILayout.Box("Drag image here", GUILayout.Width(200), GUILayout.Height(200));
            LoadImage(0, tmp);

    }

    public Texture2D[] GetTextures()
    {
        return textures;
    }

    //gets that folder the object is stored in externally
    public static void GetParentDir(string m_path)
    {
        System.IO.DirectoryInfo parentDir = Directory.GetParent(m_path);
        parentDirectory = parentDir.FullName;
    }

    public string GetParent()
    {
        return parentDirectory;
    }
}
